using AutoMapper;
using Supermarket.API.Resources.Categories;
using Supermarket.API.Resources.Products;
using Supermarket.Domain.Entities;

namespace Supermarket.API.Mapping.Extensions
{
    public static class EntityToResourceMappingExtension
    {

        public static CategoryResource ToResource(this Category category, IMapper mapper)
        {
            return mapper.Map<Category, CategoryResource>(category);
        }

        public static ProductResource ToResource(this Product product, IMapper mapper)
        {
            return mapper.Map<Product, ProductResource>(product);
        }
    }
}