using System.ComponentModel.DataAnnotations;

namespace Supermarket.API.Resources.Categories
{
    public class SaveCategoryResource
    {
        [Required(ErrorMessage = "Name is required.")]
        [MaxLength(30, ErrorMessage = "Maximum name length is 30.")]
        public string Name { get; set; }
    }
}