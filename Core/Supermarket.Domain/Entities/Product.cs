using Supermarket.Domain.Entities.Base;
using Supermarket.Domain.Enumerations;

namespace Supermarket.Domain.Entities
{
    public class Product : EntityBase
    {
        public string Name { get; set; }
        public short QuantityInPackage { get; set; }
        public EUnitOfMeasurement UnitOfMeasurement { get; set; }

        // Relationships
        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}