using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Supermarket.Application.Repositories;
using Supermarket.Application.Services.Base;
using Supermarket.Application.Services.Contracts;
using Supermarket.Domain.Entities;

namespace Supermarket.Application.Services
{
    public class ProductService : ServiceBase<Product, IProductRepository>, IProductService
    {
        public ProductService(IUnitOfWork unitOfWork, IProductRepository repository)
            : base(unitOfWork, repository)
        {
        }

        public override async Task<IEnumerable<Product>> ListAsync()
        {
            var query = UnitOfWork.ProductRepository.Get();
            query = query.Include(c => c.Category);
            var models = await query.ToListAsync();
            return models;
        }
    }
}