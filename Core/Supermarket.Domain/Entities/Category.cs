using System.Collections.Generic;
using Supermarket.Domain.Entities.Base;

namespace Supermarket.Domain.Entities
{
    public class Category : EntityBase
    {
        public Category()
        {
            Products = new List<Product>();
        }

        public string Name { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}