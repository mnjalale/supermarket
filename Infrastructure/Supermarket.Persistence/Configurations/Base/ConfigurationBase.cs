using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Supermarket.Domain.Entities.Base;

namespace Supermarket.Persistence.Configurations.Base
{
    public class ConfigurationBase<T> : IEntityTypeConfiguration<T> where T : EntityBase
    {
        public virtual void Configure(EntityTypeBuilder<T> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Property(c => c.CreatedBy).IsRequired();
            builder.Property(c => c.CreatedAt).IsRequired();
            builder.Property(c => c.ModifiedBy).IsRequired();
            builder.Property(c => c.ModifiedAt).IsRequired();
            builder.HasQueryFilter(c => !c.IsDeleted);

        }
    }
}