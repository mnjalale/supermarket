using Supermarket.Application.Repositories;
using Supermarket.Domain.Entities;
using Supermarket.Persistence.Repositories.Base;

namespace Supermarket.Persistence.Repositories
{
    public class CategoryRepository : RepositoryBase<Category>, ICategoryRepository
    {
        public CategoryRepository(AppDbContext context) : base(context)
        {
        }

    }
}