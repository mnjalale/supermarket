using AutoMapper;
using Supermarket.API.Resources.Categories;
using Supermarket.API.Resources.Products;
using Supermarket.Domain.Entities;
using Supermarket.Domain.Extensions;

namespace Supermarket.API.Mapping
{
    public class EntityToResourceProfile : Profile
    {
        public EntityToResourceProfile()
        {
            CreateMap<Category, CategoryResource>();
            CreateMap<Product, ProductResource>()
                .ForMember(resource => resource.UnitOfMeasurement,
                            opt => opt.MapFrom(product => product.UnitOfMeasurement.ToDescriptionString()));
        }

    }
}