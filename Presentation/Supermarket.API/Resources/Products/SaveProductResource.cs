using System.ComponentModel.DataAnnotations;

namespace Supermarket.API.Resources.Products
{
    public class SaveProductResource
    {
        [Required(ErrorMessage = "Name is required.")]
        [MaxLength(30, ErrorMessage = "Maximum name length is 30.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Category Id is required.")]
        public int CategoryId { get; set; }

        [Required(ErrorMessage = "Quantity in Package is required.")]
        public short QuantityInPackage { get; set; }

        [Required(ErrorMessage = "Unit of Measurement is required.")]
        public string UnitOfMeasurement { get; set; }
    }
}