using System.ComponentModel.DataAnnotations;

namespace Supermarket.API.Resources.Auth
{
    public class LoginResource
    {
        [Required(ErrorMessage = "Username is required.")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        public string Password { get; set; }
    }
}