using System;
using System.Threading.Tasks;
using Supermarket.Application.Services.Communication;
using Supermarket.Application.Services.Contracts.Base;
using Supermarket.Domain.Entities;

namespace Supermarket.Application.Services.Contracts
{
    public interface ICustomerService : IServiceBase<Customer>
    {
        [Obsolete("This should not be used for customers", true)]
        new Task<ServiceResponse<Customer>> AddAsync(Customer model);

        Task<ServiceResponse<Customer>> AddAsync(Customer model, AppUser user, string password);
    }
}