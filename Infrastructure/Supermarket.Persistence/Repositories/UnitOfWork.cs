using System.Threading.Tasks;
using Supermarket.Application.Repositories;

namespace Supermarket.Persistence.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;

        public UnitOfWork(
            AppDbContext context,
            ICategoryRepository categoryRepository,
            ICustomerRepository customerRepository,
            IProductRepository productRepository
            )
        {
            _context = context;
            CategoryRepository = categoryRepository;
            CustomerRepository = customerRepository;
            ProductRepository = productRepository;
        }

        public ICategoryRepository CategoryRepository { get; private set; }
        public ICustomerRepository CustomerRepository { get; private set; }
        public IProductRepository ProductRepository { get; private set; }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}