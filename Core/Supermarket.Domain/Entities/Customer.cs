using Supermarket.Domain.Entities.Base;

namespace Supermarket.Domain.Entities
{
    public class Customer : EntityBase
    {
        public string AppUserId { get; set; }
        public string Location { get; set; }
        public AppUser AppUser { get; set; }
    }
}