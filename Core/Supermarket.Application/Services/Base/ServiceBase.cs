using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Supermarket.Application.Repositories;
using Supermarket.Application.Repositories.Base;
using Supermarket.Application.Services.Communication;
using Supermarket.Application.Services.Contracts.Base;
using Supermarket.Domain.Entities.Base;

namespace Supermarket.Application.Services.Base
{
    public abstract class ServiceBase<M, R> : IServiceBase<M>
                                    where M : EntityBase
                                    where R : IRepositoryBase<M>
    {
        protected readonly IUnitOfWork UnitOfWork;
        private readonly IRepositoryBase<M> _repository;
        private readonly string _modelType;

        public ServiceBase(IUnitOfWork unitOfWork, R repository)
        {
            UnitOfWork = unitOfWork;
            _repository = repository;
            _modelType = typeof(M).Name;
        }

        public virtual async Task<ServiceResponse<M>> DeleteAsync(int id)
        {
            var existingModel = await _repository.FindByIdAsync(id);

            if (existingModel == null)
            {
                return GetErrorResponse($"{_modelType} not found.");
            }

            try
            {
                _repository.Remove(existingModel);
                await UnitOfWork.SaveChangesAsync();
                return GetSuccessResponse(existingModel);
            }
            catch (Exception ex)
            {
                return GetErrorResponse($"An error occurred when deleting the {_modelType.ToLower()}: {ex.Message}");
            }
        }

        public virtual async Task<IEnumerable<M>> ListAsync()
        {
            var models = await _repository.Get().ToListAsync();
            return models;
        }

        public virtual async Task<ServiceResponse<M>> AddAsync(M model)
        {
            try
            {
                await _repository.AddAsync(model);
                await UnitOfWork.SaveChangesAsync();
                return GetSuccessResponse(model);
            }
            catch (Exception ex)
            {
                return GetErrorResponse($"An error occurred when saving the {_modelType.ToLower()}: {ex.Message}");
            }
        }

        public virtual async Task<ServiceResponse<M>> UpdateAsync(M model)
        {
            try
            {
                // var existingModel = await _repository.FindByIdAsync(model.Id);

                // if (existingModel == null)
                // {
                //     return new ServiceResponse<M>($"{_modelType} not found");
                // }

                _repository.Update(model);
                await UnitOfWork.SaveChangesAsync();
                return GetSuccessResponse(model);
            }
            catch (Exception ex)
            {
                return GetErrorResponse($"An error occurred when updating the {_modelType.ToLower()}: {ex.Message}");
            }
        }

        protected ServiceResponse<M> GetErrorResponse(string errorMessage)
        {
            return new ServiceResponse<M>(errorMessage);
        }

        protected ServiceResponse<M> GetSuccessResponse(M model)
        {
            return new ServiceResponse<M>(model);
        }
    }
}