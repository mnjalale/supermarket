using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Supermarket.Application.Repositories.Base;
using Supermarket.Domain.Entities.Base;

namespace Supermarket.Persistence.Repositories.Base
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : EntityBase
    {
        protected readonly AppDbContext Context;
        protected readonly DbSet<T> DbSet;

        public RepositoryBase(AppDbContext context)
        {
            Context = context;
            DbSet = context.Set<T>();
        }

        public virtual async Task AddAsync(T model)
        {
            var now = DateTimeOffset.UtcNow;
            model.CreatedAt = now;
            model.ModifiedAt = now;

            await DbSet.AddAsync(model);
        }

        public void Update(T model)
        {
            model.ModifiedAt = DateTimeOffset.UtcNow;

            DbSet.Update(model);
        }

        public IQueryable<T> Get(
            Expression<Func<T, bool>> filter = null
            , Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null
            , string includeProperties = ""
            , bool showDeleted = false)
        {
            IQueryable<T> models = DbSet;

            if (filter != null)
            {
                models = models.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                models = models.Include(includeProperty);
            }

            if (!showDeleted)
            {
                models = models.Where(c => !c.IsDeleted);
            }

            return orderBy != null ? orderBy(models) : models;
        }

        public async Task<T> FindByIdAsync(int id)
        {
            var model = await DbSet.FindAsync(id);
            return model;
        }

        public void Remove(T model)
        {
            model.IsDeleted = true;
            model.ModifiedAt = DateTimeOffset.UtcNow;
            DbSet.Update(model);
        }

    }
}