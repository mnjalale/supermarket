using System.Threading.Tasks;

namespace Supermarket.Application.Repositories
{
    public interface IUnitOfWork
    {
        // Repositories
        ICategoryRepository CategoryRepository { get; }
        ICustomerRepository CustomerRepository { get; }
        IProductRepository ProductRepository { get; }

        // Functions
        Task SaveChangesAsync();
    }
}