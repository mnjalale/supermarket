using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Supermarket.Domain.Entities;
using Supermarket.Persistence.Configurations.Base;

namespace Supermarket.Persistence.Configurations
{
    public class CustomerConfiguration : ConfigurationBase<Customer>
    {
        public override void Configure(EntityTypeBuilder<Customer> builder)
        {
            base.Configure(builder);

            // Fields
            builder.ToTable("Customers");
            builder.HasOne(c => c.AppUser);
        }
    }
}