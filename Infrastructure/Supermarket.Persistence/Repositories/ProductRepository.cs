using Supermarket.Application.Repositories;
using Supermarket.Domain.Entities;
using Supermarket.Persistence.Repositories.Base;

namespace Supermarket.Persistence.Repositories
{
    public class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(AppDbContext context) : base(context)
        {
        }
    }
}