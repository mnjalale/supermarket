using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Supermarket.Domain.Entities;
using Supermarket.Persistence.Configurations.Base;

namespace Supermarket.Persistence.Configurations
{
    public class CategoryConfiguration : ConfigurationBase<Category>
    {
        public override void Configure(EntityTypeBuilder<Category> builder)
        {
            base.Configure(builder);

            // Fields
            builder.ToTable("Categories");
            builder.Property(c => c.Name).IsRequired().HasMaxLength(30);
            builder.HasMany(c => c.Products).WithOne(c => c.Category).HasForeignKey(c => c.CategoryId).OnDelete(DeleteBehavior.Cascade);

            // Seed data
            var now = DateTimeOffset.Now;
            builder.HasData(
                new Category
                {
                    Id = 100,
                    Name = "Fruits and Vegetables",
                    CreatedAt = now,
                    CreatedBy = "mnjalale",
                    ModifiedAt = now,
                    ModifiedBy = "mnjalale"
                },
                new Category
                {
                    Id = 101,
                    Name = "Dairy",
                    CreatedAt = now,
                    CreatedBy = "mnjalale",
                    ModifiedAt = now,
                    ModifiedBy = "mnjalale"
                }
            );
        }
    }
}