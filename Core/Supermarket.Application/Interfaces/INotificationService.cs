using System.Threading.Tasks;

namespace Supermarket.Application.Interfaces
{
    public interface INotificationService
    {
        Task SendAsync(string message);
    }
}