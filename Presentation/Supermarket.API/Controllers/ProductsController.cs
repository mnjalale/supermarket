using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Supermarket.API.Extensions;
using Supermarket.API.Mapping.Extensions;
using Supermarket.API.Resources.Products;
using Supermarket.Application.Services.Contracts;

namespace Supermarket.API.Controllers
{
    [Route("/api/[controller]")]
    public class ProductsController : Base.ControllerBase
    {
        private readonly IProductService _productService;

        public ProductsController(IProductService productService, IMapper mapper)
            : base(mapper)
        {
            _productService = productService;
        }

        [HttpGet]
        public async Task<IEnumerable<ProductResource>> GetAllAsync()
        {
            var products = await _productService.ListAsync();
            return products.Select(c => c.ToResource(Mapper));
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] SaveProductResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var product = resource.ToEntity(Mapper);
            var result = await _productService.AddAsync(product);

            if (!result.Success)
            {
                return BadRequest(result.Message);
            }

            var productResource = result.Model.ToResource(Mapper);
            return Ok(productResource);

        }

        [HttpPut]
        public async Task<IActionResult> PutAsync([FromBody] ProductResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var product = resource.ToEntity(Mapper);

            var result = await _productService.UpdateAsync(product);

            if (!result.Success)
            {
                return BadRequest(result.Message);
            }

            var productResource = result.Model.ToResource(Mapper);
            return Ok(productResource);
        }

        [HttpDelete("{id}")]
        public virtual async Task<IActionResult> DeleteAsync(int id)
        {
            var result = await _productService.DeleteAsync(id);

            if (!result.Success)
            {
                return BadRequest(result.Message);
            }

            var product = result.Model.ToResource(Mapper);
            return Ok(product);
        }
    }
}