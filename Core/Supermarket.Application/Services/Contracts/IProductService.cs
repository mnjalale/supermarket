using Supermarket.Application.Services.Contracts.Base;
using Supermarket.Domain.Entities;

namespace Supermarket.Application.Services.Contracts
{
    public interface IProductService : IServiceBase<Product>
    {

    }
}