using System;
using System.Threading.Tasks;
using Supermarket.Application.Repositories.Base;
using Supermarket.Domain.Entities;

namespace Supermarket.Application.Repositories
{
    public interface ICustomerRepository : IRepositoryBase<Customer>
    {
    }
}