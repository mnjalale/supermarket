using Supermarket.Application.Repositories;
using Supermarket.Domain.Entities;
using Supermarket.Persistence.Repositories.Base;

namespace Supermarket.Persistence.Repositories
{
    public class CustomerRepository : RepositoryBase<Customer>, ICustomerRepository
    {
        public CustomerRepository(AppDbContext context)
            : base(context)
        {
        }
    }
}