using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Supermarket.API.Extensions;
using Supermarket.API.Mapping.Extensions;
using Supermarket.API.Resources.Categories;
using Supermarket.Application.Services.Contracts;

namespace Supermarket.API.Controllers
{
    // [Authorize(Policy = "ApiUser")]
    [Route("/api/[controller]")]
    public class CategoriesController : Base.ControllerBase
    {
        private readonly ICategoryService _categoryService;

        public CategoriesController(ICategoryService categoryService, IMapper mapper)
            : base(mapper)
        {
            _categoryService = categoryService;
        }

        [HttpGet]
        public async Task<IEnumerable<CategoryResource>> GetAllAsync()
        {
            var categories = await _categoryService.ListAsync();
            return categories.Select(c => c.ToResource(Mapper));
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] SaveCategoryResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var category = resource.ToEntity(Mapper);
            var result = await _categoryService.AddAsync(category);

            if (!result.Success)
            {
                return BadRequest(result.Message);
            }

            var categoryResource = result.Model.ToResource(Mapper);
            return Ok(categoryResource);
        }

        [HttpPut]
        public async Task<IActionResult> PutAsync([FromBody] CategoryResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var category = resource.ToEntity(Mapper);

            var result = await _categoryService.UpdateAsync(category);

            if (!result.Success)
            {
                return BadRequest(result.Message);
            }

            var categoryResource = result.Model.ToResource(Mapper);
            return Ok(categoryResource);
        }

        [HttpDelete("{id}")]
        public virtual async Task<IActionResult> DeleteAsync(int id)
        {
            var result = await _categoryService.DeleteAsync(id);

            if (!result.Success)
            {
                return BadRequest(result.Message);
            }

            var category = result.Model.ToResource(Mapper);
            return Ok(category);
        }

    }
}