using Supermarket.API.Resources.Categories;

namespace Supermarket.API.Resources.Products
{
    public class ProductResource
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public short QuantityInPackage { get; set; }
        public string UnitOfMeasurement { get; set; }

        public CategoryResource Category { get; set; }
    }
}