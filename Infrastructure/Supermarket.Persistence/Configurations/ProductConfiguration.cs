using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Supermarket.Domain.Entities;
using Supermarket.Domain.Enumerations;
using Supermarket.Persistence.Configurations.Base;

namespace Supermarket.Persistence.Configurations
{
    public class ProductConfiguration : ConfigurationBase<Product>
    {
        public override void Configure(EntityTypeBuilder<Product> builder)
        {
            base.Configure(builder);

            // Fields
            builder.ToTable("Products");
            builder.Property(c => c.Name).IsRequired().HasMaxLength(30);
            builder.Property(c => c.QuantityInPackage).IsRequired();
            builder.Property(c => c.UnitOfMeasurement).IsRequired();
            builder.HasOne(c => c.Category).WithMany(c => c.Products).HasForeignKey(c => c.CategoryId);

            // Seed data
            var now = DateTimeOffset.Now;
            builder.HasData(
                new Product
                {
                    Id = 100,
                    Name = "Apple",
                    QuantityInPackage = 1,
                    UnitOfMeasurement = EUnitOfMeasurement.Unity,
                    CategoryId = 100,
                    CreatedAt = now,
                    CreatedBy = "mnjalale",
                    ModifiedAt = now,
                    ModifiedBy = "mnjalale"
                },
                new Product
                {
                    Id = 101,
                    Name = "Milk",
                    QuantityInPackage = 2,
                    UnitOfMeasurement = EUnitOfMeasurement.Liter,
                    CategoryId = 101,
                    CreatedAt = now,
                    CreatedBy = "mnjalale",
                    ModifiedAt = now,
                    ModifiedBy = "mnjalale"
                }
            );
        }
    }
}