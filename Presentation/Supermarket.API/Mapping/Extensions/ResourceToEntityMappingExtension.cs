using AutoMapper;
using Supermarket.API.Resources.Categories;
using Supermarket.API.Resources.Customers;
using Supermarket.API.Resources.Products;
using Supermarket.Domain.Entities;

namespace Supermarket.API.Mapping.Extensions
{
    public static class ResourceToEntityMappingExtension
    {
        // Category
        public static Category ToEntity(this CategoryResource resource, IMapper mapper)
        {
            return mapper.Map<CategoryResource, Category>(resource);
        }

        public static Category ToEntity(this SaveCategoryResource resource, IMapper mapper)
        {
            return mapper.Map<SaveCategoryResource, Category>(resource);
        }

        // Product
        public static Product ToEntity(this ProductResource resource, IMapper mapper)
        {
            return mapper.Map<ProductResource, Product>(resource);
        }

        public static Product ToEntity(this SaveProductResource resource, IMapper mapper)
        {
            return mapper.Map<SaveProductResource, Product>(resource);
        }

        // Customer
        public static Customer ToCustomerEntity(this SaveCustomerResource resource, IMapper mapper)
        {
            return mapper.Map<SaveCustomerResource, Customer>(resource);
        }

        // App User
        public static AppUser ToAppUserEntity(this SaveCustomerResource resource, IMapper mapper)
        {
            return mapper.Map<SaveCustomerResource, AppUser>(resource);
        }
    }
}