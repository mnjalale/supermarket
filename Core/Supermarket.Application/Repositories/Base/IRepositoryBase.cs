using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Supermarket.Domain.Entities.Base;

namespace Supermarket.Application.Repositories.Base
{
    public interface IRepositoryBase<T> where T : EntityBase
    {
        IQueryable<T> Get(
             Expression<Func<T, bool>> filter = null
            , Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null
            , string includeProperties = ""
            , bool showDeleted = false
        );
        Task<T> FindByIdAsync(int id);
        Task AddAsync(T model);
        void Update(T model);
        void Remove(T model);
    }
}