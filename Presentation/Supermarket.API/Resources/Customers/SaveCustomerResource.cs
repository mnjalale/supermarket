using System.ComponentModel.DataAnnotations;

namespace Supermarket.API.Resources.Customers
{
    public class SaveCustomerResource
    {
        [Required(ErrorMessage = "Email is required.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        public string Password { get; set; }

        [Required(ErrorMessage = "First Name is required.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name is required.")]
        public string LastName { get; set; }
        public string Location { get; set; }
    }
}