using System.Threading.Tasks;
using Supermarket.Application.Interfaces;

namespace Supermarket.Infrastructure
{
    public class NotificationService : INotificationService
    {
        public Task SendAsync(string message)
        {
            return Task.CompletedTask;
        }
    }
}