using Supermarket.Application.Repositories;
using Supermarket.Application.Services.Base;
using Supermarket.Application.Services.Contracts;
using Supermarket.Domain.Entities;

namespace Supermarket.Application.Services
{
    public class CategoryService : ServiceBase<Category, ICategoryRepository>, ICategoryService
    {
        public CategoryService(IUnitOfWork unitOfWork, ICategoryRepository repository)
            : base(unitOfWork, repository)
        {
        }
    }
}