using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Supermarket.API.Extensions;
using Supermarket.API.Mapping.Extensions;
using Supermarket.API.Resources.Customers;
using Supermarket.Application.Services.Contracts;

namespace Supermarket.API.Controllers
{
    [Route("/api/[controller]")]

    public class AccountsController : Base.ControllerBase
    {
        private readonly ICustomerService _customerService;
        public AccountsController(IMapper mapper, ICustomerService customerService)
            : base(mapper)
        {
            _customerService = customerService;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]SaveCustomerResource model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var user = model.ToAppUserEntity(Mapper);
            var customer = model.ToCustomerEntity(Mapper);

            var result = await _customerService.AddAsync(customer, user, model.Password);

            if (!result.Success)
            {
                return BadRequest(result.Message);
            }

            return Ok(model);
        }
    }
}