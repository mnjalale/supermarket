using System.ComponentModel.DataAnnotations;

namespace Supermarket.API.Resources.Categories
{
    public class CategoryResource
    {
        [Required(ErrorMessage = "Id is required.")]
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }

    }
}