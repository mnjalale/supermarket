using System.Collections.Generic;
using System.Threading.Tasks;
using Supermarket.Application.Services.Communication;
using Supermarket.Domain.Entities.Base;

namespace Supermarket.Application.Services.Contracts.Base
{
    public interface IServiceBase<T> where T : EntityBase
    {
        Task<IEnumerable<T>> ListAsync();
        Task<ServiceResponse<T>> AddAsync(T model);
        Task<ServiceResponse<T>> UpdateAsync(T model);
        Task<ServiceResponse<T>> DeleteAsync(int id);
    }
}