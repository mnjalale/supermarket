using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Supermarket.Application.Repositories;
using Supermarket.Application.Services.Base;
using Supermarket.Application.Services.Communication;
using Supermarket.Application.Services.Contracts;
using Supermarket.Domain.Entities;

namespace Supermarket.Application.Services
{
    public class CustomerService : ServiceBase<Customer, ICustomerRepository>, ICustomerService
    {
        private UserManager<AppUser> _userManager;
        public CustomerService(IUnitOfWork unitOfWork, ICustomerRepository repository, UserManager<AppUser> userManager)
            : base(unitOfWork, repository)
        {
            _userManager = userManager;
        }


        public async Task<ServiceResponse<Customer>> AddAsync(Customer model, AppUser user, string password)
        {
            try
            {
                // Add App User
                var result = await _userManager.CreateAsync(user, password);
                if (!result.Succeeded)
                {
                    var errorMessage = "";

                    foreach (var error in result.Errors)
                    {
                        errorMessage += error.Description + "; ";
                    }

                    throw new Exception(errorMessage);
                }

                // Add Customer
                try
                {
                    model.AppUserId = user.Id;
                    model.CreatedBy = "Test User";
                    model.ModifiedBy = "Test User";
                    await UnitOfWork.CustomerRepository.AddAsync(model);
                    await UnitOfWork.SaveChangesAsync();
                    return GetSuccessResponse(model);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            catch (Exception ex)
            {
                return GetErrorResponse($"An error occurred when saving the customer: {ex.Message}");
            }
        }

    }
}