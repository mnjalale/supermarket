﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Supermarket.Persistence.Migrations
{
    public partial class AddCustomerTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: false),
                    ModifiedAt = table.Column<DateTimeOffset>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    AppUserId = table.Column<string>(nullable: true),
                    Location = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customers_AspNetUsers_AppUserId",
                        column: x => x.AppUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "CreatedAt", "ModifiedAt" },
                values: new object[] { new DateTimeOffset(new DateTime(2019, 6, 23, 14, 55, 27, 350, DateTimeKind.Unspecified).AddTicks(7571), new TimeSpan(0, 3, 0, 0, 0)), new DateTimeOffset(new DateTime(2019, 6, 23, 14, 55, 27, 350, DateTimeKind.Unspecified).AddTicks(7571), new TimeSpan(0, 3, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "CreatedAt", "ModifiedAt" },
                values: new object[] { new DateTimeOffset(new DateTime(2019, 6, 23, 14, 55, 27, 350, DateTimeKind.Unspecified).AddTicks(7571), new TimeSpan(0, 3, 0, 0, 0)), new DateTimeOffset(new DateTime(2019, 6, 23, 14, 55, 27, 350, DateTimeKind.Unspecified).AddTicks(7571), new TimeSpan(0, 3, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "CreatedAt", "ModifiedAt" },
                values: new object[] { new DateTimeOffset(new DateTime(2019, 6, 23, 14, 55, 27, 358, DateTimeKind.Unspecified).AddTicks(9909), new TimeSpan(0, 3, 0, 0, 0)), new DateTimeOffset(new DateTime(2019, 6, 23, 14, 55, 27, 358, DateTimeKind.Unspecified).AddTicks(9909), new TimeSpan(0, 3, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "CreatedAt", "ModifiedAt" },
                values: new object[] { new DateTimeOffset(new DateTime(2019, 6, 23, 14, 55, 27, 358, DateTimeKind.Unspecified).AddTicks(9909), new TimeSpan(0, 3, 0, 0, 0)), new DateTimeOffset(new DateTime(2019, 6, 23, 14, 55, 27, 358, DateTimeKind.Unspecified).AddTicks(9909), new TimeSpan(0, 3, 0, 0, 0)) });

            migrationBuilder.CreateIndex(
                name: "IX_Customers_AppUserId",
                table: "Customers",
                column: "AppUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "CreatedAt", "ModifiedAt" },
                values: new object[] { new DateTimeOffset(new DateTime(2019, 6, 23, 12, 37, 34, 562, DateTimeKind.Unspecified).AddTicks(7681), new TimeSpan(0, 3, 0, 0, 0)), new DateTimeOffset(new DateTime(2019, 6, 23, 12, 37, 34, 562, DateTimeKind.Unspecified).AddTicks(7681), new TimeSpan(0, 3, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "CreatedAt", "ModifiedAt" },
                values: new object[] { new DateTimeOffset(new DateTime(2019, 6, 23, 12, 37, 34, 562, DateTimeKind.Unspecified).AddTicks(7681), new TimeSpan(0, 3, 0, 0, 0)), new DateTimeOffset(new DateTime(2019, 6, 23, 12, 37, 34, 562, DateTimeKind.Unspecified).AddTicks(7681), new TimeSpan(0, 3, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "CreatedAt", "ModifiedAt" },
                values: new object[] { new DateTimeOffset(new DateTime(2019, 6, 23, 12, 37, 34, 569, DateTimeKind.Unspecified).AddTicks(1282), new TimeSpan(0, 3, 0, 0, 0)), new DateTimeOffset(new DateTime(2019, 6, 23, 12, 37, 34, 569, DateTimeKind.Unspecified).AddTicks(1282), new TimeSpan(0, 3, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "CreatedAt", "ModifiedAt" },
                values: new object[] { new DateTimeOffset(new DateTime(2019, 6, 23, 12, 37, 34, 569, DateTimeKind.Unspecified).AddTicks(1282), new TimeSpan(0, 3, 0, 0, 0)), new DateTimeOffset(new DateTime(2019, 6, 23, 12, 37, 34, 569, DateTimeKind.Unspecified).AddTicks(1282), new TimeSpan(0, 3, 0, 0, 0)) });
        }
    }
}
