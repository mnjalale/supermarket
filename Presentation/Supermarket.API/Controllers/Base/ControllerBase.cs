using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace Supermarket.API.Controllers.Base
{
    public abstract class ControllerBase : Controller
    // <M, S> : Controller
    //                                     where M : ModelBase
    //                                     where S : IServiceBase<M>
    {
        protected IMapper Mapper;
        // private readonly IServiceBase<M> _service;

        public ControllerBase(IMapper mapper) //, IServiceBase<M> service)
        {
            Mapper = mapper;
            // _service = service;
        }


        // [HttpGet]
        // public async Task<IEnumerable<CategoryResource>> GetAllAsync()
        // {
        //     var categories = await _categoryService.ListAsync();
        //     return categories.Select(c => Mapper.Map<Category, CategoryResource>(c));
        // }

        // [HttpPost]
        // public async Task<IActionResult> PostAsync([FromBody] SaveCategoryResource resource)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState.GetErrorMessages());
        //     }

        //     var category = Mapper.Map<SaveCategoryResource, Category>(resource);
        //     var result = await _categoryService.SaveAsync(category);

        //     if (!result.Success)
        //     {
        //         return BadRequest(result.Message);
        //     }

        //     var categoryResource = Mapper.Map<Category, CategoryResource>(result.Model);
        //     return Ok(categoryResource);

        // }

        // [HttpPut]
        // public async Task<IActionResult> PutAsync([FromBody] CategoryResource resource)
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState.GetErrorMessages());
        //     }

        //     var category = Mapper.Map<CategoryResource, Category>(resource);

        //     var result = await _categoryService.UpdateAsync(category);

        //     if (!result.Success)
        //     {
        //         return BadRequest(result.Message);
        //     }

        //     var categoryResource = Mapper.Map<Category, CategoryResource>(result.Model);
        //     return Ok(categoryResource);
        // }

        // [HttpDelete("{id}")]
        // public virtual async Task<IActionResult> DeleteAsync(int id)
        //         {
        //             var result = await _service.DeleteAsync(id);

        //             if (!result.Success)
        //             {
        //                 return BadRequest(result.Message);
        //     }

        //     var category = Mapper.Map<Category, CategoryResource>(result.Model);
        //             return Ok(category);
        // }

    }
}