using Supermarket.Application.Repositories.Base;
using Supermarket.Domain.Entities;

namespace Supermarket.Application.Repositories
{
    public interface ICategoryRepository : IRepositoryBase<Category>
    {

    }
}