using AutoMapper;
using Supermarket.API.Resources.Categories;
using Supermarket.API.Resources.Customers;
using Supermarket.API.Resources.Products;
using Supermarket.Domain.Entities;

namespace Supermarket.API.Mapping
{
    public class ResourceToEntityProfile : Profile
    {
        public ResourceToEntityProfile()
        {
            // Category
            CreateMap<CategoryResource, Category>();
            CreateMap<SaveCategoryResource, Category>();

            // Customer
            CreateMap<SaveCustomerResource, AppUser>()
                .ForMember(au => au.UserName, map => map.MapFrom(c => c.Email));
            CreateMap<SaveCustomerResource, Customer>();

            //Product
            CreateMap<ProductResource, Product>();
            CreateMap<SaveProductResource, Product>();
        }
    }
}