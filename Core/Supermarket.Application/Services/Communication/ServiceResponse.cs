using Supermarket.Domain.Entities.Base;

namespace Supermarket.Application.Services.Communication
{
    public class ServiceResponse<T> where T : EntityBase
    {
        public bool Success { get; private set; }
        public string Message { get; private set; }
        public T Model { get; private set; }

        private ServiceResponse(bool success, string message, T model)
        {
            Success = success;
            Message = message;
            Model = model;
        }

        /// <summary>
        /// Creates a success response.
        /// </summary>
        /// <param name="model">Saved model.</param>
        /// <returns>Response.</returns>
        public ServiceResponse(T model) : this(true, string.Empty, model)
        { }

        /// <summary>
        /// Creates am error response.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <returns>Response.</returns>
        public ServiceResponse(string message) : this(false, message, null)
        { }

    }
}